package cn.lingwan.hdfs.demo1;



import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HdfsOperateStudy {

        
    @Test
    public void getFileSystem1() throws IOException {
    
        Configuration configuration = new Configuration();

    
        configuration.set("fs.defaultFS","hdfs://node01:8020/");

        FileSystem fileSystem = FileSystem.get(configuration);

        System.out.println(fileSystem.toString());
    }

        
    @Test
    public void getHdfs2() throws URISyntaxException, IOException {
    
    
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());
        System.out.println(fileSystem.toString());
    }

        
    @Test
    public void getHdfs3() throws IOException {
        Configuration configuration = new Configuration();
        configuration.get("fs.defaultFS","hdfs://node01:8020");
        FileSystem fileSystem = FileSystem.newInstance(configuration);
        System.out.println(fileSystem.toString());
    }

        
    @Test
    public void getAllHdfsFilePath() throws Exception {

        Path path = new Path("/");
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());

        FileStatus[] fileStatuses = fileSystem.listStatus(path);

        for (FileStatus fileStatus : fileStatuses) {
            if (fileStatus.isDirectory()){
                getDirctoryFile(fileSystem,fileStatus);
            }else{
                Path path1=fileStatus.getPath();
                System.out.println(path1.toString());
            }
        }
        fileSystem.close();
    }

    private void getDirctoryFile(FileSystem fileSystem, FileStatus fileStatus) throws IOException {
        Path path = fileStatus.getPath();
        FileStatus[] fileStatuses = fileSystem.listStatus(path);
        for (FileStatus status : fileStatuses) {
            if (status.isDirectory()){
                getDirctoryFile(fileSystem,status);
            }else{
                System.out.println(status.getPath().toString());
            }
        }
    }

        
    @Test
    public void listHdfsFiles() throws Exception {
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());
        Path path = new Path("/");
        RemoteIterator<LocatedFileStatus> locatedFileStatusRemoteIterator = fileSystem.listFiles(path, true);
        while (locatedFileStatusRemoteIterator.hasNext()){
            LocatedFileStatus next=locatedFileStatusRemoteIterator.next();
            Path path1 = next.getPath();
            System.out.println(path1.toString());
        }
        fileSystem.close();
    }

        
    @Test
    public void copyHdfsToLocal() throws Exception {
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());
        Path path = new Path("hdfs://node01:8020/tmp/install.log.syslog");
    
        FSDataInputStream inputStream = fileSystem.open(path);
    
        FileOutputStream outputStream = new FileOutputStream(new File("c:\\test\\myinstall.log"));

        IOUtils.copy(inputStream,outputStream);
        IOUtils.closeQuietly(inputStream);
        IOUtils.closeQuietly(outputStream);
        fileSystem.close();
    }

        
    @Test
    public void createHdfsDir() throws Exception{
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());

        fileSystem.mkdirs(new Path("/abc/bbc/ddd"));
        fileSystem.close();
    }

        
    @Test
    public void uploadFileToHdfs() throws Exception{
        FileSystem fileSystem = FileSystem.get(new URI("hdfs://node01:8020"), new Configuration());
        fileSystem.copyFromLocalFile(false,new Path("file:///c:\\test\\myinstall.log"),new Path("/abc/bbc/ddd"));
        fileSystem.close();
    }
}
