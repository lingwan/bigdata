package cn.lingwan.demo1;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.testng.annotations.Test;

public class ZkStudy {
       
    @Test
    public void createNode() throws Exception {
   
        String connectString="node01:2181,node02:2181,node03:2181";
   
        RetryPolicy retryPolicy=new ExponentialBackoffRetry(3000,3);
   
        CuratorFramework client=CuratorFrameworkFactory.newClient(connectString,retryPolicy);
   
        client.start();
   
        client.create().creatingParentsIfNeeded()
                .withMode(CreateMode.PERSISTENT)
                .forPath("/node01","helloworld".getBytes());
        client.close();
    }

       
    @Test
    public void createTempNode() throws Exception {
   
        CuratorFramework client=CuratorFrameworkFactory.newClient("node01:2181,node02:2181,node03:2181",new ExponentialBackoffRetry(5000,5));
        client.start();
        client.create().creatingParentsIfNeeded()
                .withMode(CreateMode.EPHEMERAL)
                .forPath("/myTempNode","tempNode".getBytes());
        Thread.sleep(5000);
        client.close();
    }

       
    @Test
    public void updateNodeData() throws Exception {
   
        CuratorFramework client=CuratorFrameworkFactory.newClient("node01:2181",new ExponentialBackoffRetry(5000,3));
        client.start();
   
        client.setData().forPath("/node01","word5".getBytes());
        client.close();
    }

       
    @Test
    public void getDatas() throws Exception {
   
        CuratorFramework client = CuratorFrameworkFactory.newClient("node03:2181", new ExponentialBackoffRetry(6000, 6));
        client.start();
        byte[] bytes = client.getData().forPath("/node01");
        System.out.println(new String(bytes));
        client.close();
    }

       
    @Test
    public void watchNode() throws Exception {
   
        CuratorFramework client = CuratorFrameworkFactory.newClient("node01:2181", new ExponentialBackoffRetry(3000, 3));
        client.start();
        TreeCache cache = new TreeCache(client,"/node01");
        cache.getListenable().addListener(new TreeCacheListener() {
               
            @Override
            public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent treeCacheEvent) throws Exception {
   
                ChildData data = treeCacheEvent.getData();
                if (null!=data){
   
   
                    TreeCacheEvent.Type type = treeCacheEvent.getType();
                    switch (type){
                        case NODE_ADDED:
                            System.out.println("I watched a node to be added");
                            break;
                        case NODE_UPDATED:
                            System.out.println("I watched a node to be updated");
                            break;
                        case INITIALIZED:
                            System.out.println("I watched a node to be initialized");
                            break;
                        case NODE_REMOVED:
                            System.out.println("I watched a node to be removed");
                            break;
                        default:
                            System.out.println("nothing to do");
                            break;
                    }
                }
                
            }
        });
        cache.start();
        Thread.sleep(60000000);
    }
}

