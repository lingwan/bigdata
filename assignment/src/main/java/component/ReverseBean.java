package component;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ReverseBean implements WritableComparable<ReverseBean>{

    private String count;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public int compareTo(ReverseBean o) {
        return o.count.compareTo(this.count);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(count);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.count=dataInput.readUTF();
    }
}
