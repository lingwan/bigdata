package component;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class IpJoinBean implements WritableComparable<IpJoinBean>{
    private String user="";
    private String ip="";
    private String loginTime="";
    private String logoutTime="";
    private String type="";
    private String url="";
    private String visitTime="";

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(String logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    @Override
    public String toString() {
        return "ip:"+ip+" user:"+user+" url:"+url+" loginTime:"+loginTime+" logouttime:"+logoutTime+" visitTime:" +visitTime;
    }

    public boolean visitMatchLogin(IpJoinBean login){
        if(this.visitTime.compareTo(login.loginTime)>=0&&this.visitTime.compareTo(login.logoutTime)<=0){
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(IpJoinBean o) {
        int i = this.ip.compareTo(o.ip);
        return i;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(type);
        dataOutput.writeUTF(ip);
        dataOutput.writeUTF(user);
        dataOutput.writeUTF(url);
        dataOutput.writeUTF(loginTime);
        dataOutput.writeUTF(logoutTime);
        dataOutput.writeUTF(visitTime);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.type = dataInput.readUTF();
        this.ip = dataInput.readUTF();
        this.user = dataInput.readUTF();
        this.url = dataInput.readUTF();
        this.loginTime = dataInput.readUTF();
        this.logoutTime = dataInput.readUTF();
        this.visitTime = dataInput.readUTF();
    }

    public IpJoinBean(String ip,String user,String url,String loginTime,String logoutTime,String visitTime){
        this.ip = ip;
        this.user = user;
        this.url = url;
        this.loginTime = loginTime;
        this.logoutTime = logoutTime;
        this.visitTime = visitTime;
    }
    public IpJoinBean(){
    }
}
