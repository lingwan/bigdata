import component.IpJoinBean;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class JoinWithIpMain extends Configured implements Tool{
    @Override
    public int run(String[] strings) throws Exception {
        Job job = Job.getInstance(super.getConf(), "JoinWithIp");
        job.setInputFormatClass(TextInputFormat.class);
        Path[] paths=new Path[2];
        paths[0]=new Path("file:///C:\\test\\input\\visit.log");
        paths[1]=new Path("file:///C:\\test\\login_format");
        TextInputFormat.setInputPaths(job,paths);

        job.setMapperClass(JoinWithIpMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IpJoinBean.class);

        job.setReducerClass(JoinWithIpReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("file:///C:\\test\\join_out"));
        boolean b = job.waitForCompletion(true);

        return b?0:1;
    }

    public static void main(String[] args) throws Exception {
        int run = ToolRunner.run(new Configuration(), new JoinWithIpMain(), args);
        System.exit(run);
    }
}

