import component.ReverseBean;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

public class TopMP {

    public static class TopMapper extends Mapper<LongWritable,Text, ReverseBean,Text>{
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] split = value.toString().split("_");
            ReverseBean rb=new ReverseBean();
            rb.setCount(split[1]);
            context.write(rb,value);
        }
    }

    public static class TopReducer extends Reducer<ReverseBean,Text,Text,IntWritable> {
        int counter=0;
        @Override
        protected void reduce(ReverseBean key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            for (Text value : values) {
                if (counter<2){
                    String[] split = value.toString().split("_");
                    String url=split[0];
                    String count=split[1];
                    context.write(new Text(url),new IntWritable(Integer.parseInt(count)));
                    counter++;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("file:///c:\\test\\uv_result"));

        job.setMapperClass(TopMapper.class);
        job.setMapOutputKeyClass(ReverseBean.class);
        job.setMapOutputValueClass(Text.class);

        job.setReducerClass(TopReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("file:///C:\\test\\uv_top10"));

        job.waitForCompletion(true);
    }
}
