import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


import java.io.IOException;

public class LoginFormatMapper extends Mapper<LongWritable,Text,Text,Text> {
    Text outKey=new Text();
    Text outValue=new Text();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split(",");
        String user = line[0];
        String ip = line[1];
        String time = line[2];
        String opr = line[3];
        outKey.set(user+"_"+ip);
        outValue.set(time+"_"+opr);
        context.write(outKey,outValue);
    }
}
