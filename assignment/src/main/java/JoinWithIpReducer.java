import component.IpJoinBean;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JoinWithIpReducer extends Reducer<Text, IpJoinBean,Text,NullWritable>{
    private List<IpJoinBean> loginData = new ArrayList<IpJoinBean>();
    private List<IpJoinBean> visitData = new ArrayList<IpJoinBean>();
    @Override
    protected void reduce(Text key, Iterable<IpJoinBean> values, Context context) throws IOException, InterruptedException {

        for (IpJoinBean value : values) {
            if(value.getType().equals("visit")){
                visitData.add(new IpJoinBean(value.getIp(),value.getUser(),value.getUrl(),value.getLoginTime(),value.getLogoutTime(),value.getVisitTime()));
            }else if(value.getType().equals("login")){
                loginData.add(new IpJoinBean(value.getIp(),value.getUser(),value.getUrl(),value.getLoginTime(),value.getLogoutTime(),value.getVisitTime()));
            }
        }

        for(IpJoinBean visit:visitData){
            for(IpJoinBean login:loginData){
                if(visit.visitMatchLogin(login)){
                    context.write(new Text(visit.getIp()+"_"+login.getUser()+"_"+visit.getUrl()),NullWritable.get());
                }
            }
        }
        loginData.clear();
        visitData.clear();
    }
}
