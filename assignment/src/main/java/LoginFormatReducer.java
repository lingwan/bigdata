import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoginFormatReducer extends Reducer<Text,Text,Text,NullWritable> {
    List loginTimes = new ArrayList();
    List logoutTimes = new ArrayList();

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for(Text value :values){
            String time = value.toString().split("_")[0];
            String opr = value.toString().split("_")[1];
            if(opr.equals("1")){
                loginTimes.add(time);
            }else if(opr.equals("2")){
                logoutTimes.add(time);
            }
        }
        Collections.sort(loginTimes);
        Collections.sort(logoutTimes);
        for (int i=0;i<loginTimes.size();i++) {
            context.write(new Text(key + "_" + loginTimes.get(i) + "_" + logoutTimes.get(i)), NullWritable.get());
        }
        loginTimes.clear();
        logoutTimes.clear();
    }
}
