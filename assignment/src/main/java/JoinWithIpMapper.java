import component.IpJoinBean;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;

public class JoinWithIpMapper extends Mapper<LongWritable,Text,Text, IpJoinBean>{
    private String fileName;
    private IpJoinBean ipJoinBean=new IpJoinBean();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        FileSplit input = (FileSplit) context.getInputSplit();

        try {
            fileName = input.getPath().getName();;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if(fileName.startsWith("visit")){

            String[] parms = value.toString().split(",");
            String ip = parms[0];
            String time = parms[1];
            String url = parms[2];
            ipJoinBean.setIp(ip);
            ipJoinBean.setVisitTime(time);
            ipJoinBean.setUrl(url);
            ipJoinBean.setType("visit");

        }else{
            String[] parms = value.toString().split("_");
            String user = parms[0];
            String ip = parms[1];
            String loginTime = parms[2];
            String logoutTime = parms[3];
            ipJoinBean.setUser(user);
            ipJoinBean.setIp(ip);
            ipJoinBean.setType("login");
            ipJoinBean.setLoginTime(loginTime);
            ipJoinBean.setLogoutTime(logoutTime);

        }
        context.write(new Text(ipJoinBean.getIp()),ipJoinBean);
    }
}
