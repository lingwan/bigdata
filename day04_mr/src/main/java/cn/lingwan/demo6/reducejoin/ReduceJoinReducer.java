package cn.lingwan.demo6.reducejoin;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ReduceJoinReducer extends Reducer<Text,Text,Text,Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        String firstPart="";
        String secondPart="";
        for (Text value : values) {
            if (value.toString().startsWith("p")){
                firstPart=value.toString();
            }else {
                secondPart=value.toString();
            }

        }
        context.write(key,new Text(firstPart+"\t"+secondPart));
    }
}
