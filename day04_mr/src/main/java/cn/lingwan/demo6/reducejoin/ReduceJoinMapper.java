package cn.lingwan.demo6.reducejoin;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class ReduceJoinMapper extends Mapper<LongWritable,Text,Text,Text> {
    Text text=new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
      
        String s = value.toString();
        if (s.startsWith("p")){
      
            String[] split = s.split(",");
            text.set(split[0]);
            context.write(text,value);
        }else{
            String[] split = s.split(",");
            text.set(split[2]);
            context.write(text,value);
        }
    }
}
