package cn.lingwan.demo2.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class SortMain extends Configured implements Tool {
    @Override
    public int run(String[] strings) throws Exception {
      
        Job job = Job.getInstance(super.getConf(), "lingwan");
        job.setJarByClass(SortMain.class);

      
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("file:///c:\\test\\input\\sort.txt"));

      
        job.setMapperClass(SortMapper.class);
      
        job.setMapOutputKeyClass(PairSort.class);
        job.setOutputValueClass(Text.class);

      

      
        job.setReducerClass(SortReducer.class);
        job.setOutputKeyClass(PairSort.class);
        job.setOutputValueClass(Text.class);

      
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("file:///c:\\test\\outSort"));

      
        boolean b = job.waitForCompletion(true);
        return b?0:1;
    }

    public static void main(String[] args) throws Exception {
        int run = ToolRunner.run(new Configuration(), new SortMain(), args);
        System.exit(run);

    }
}
